#   FIND Cgreen
#   CGREEN_INCLUDE_DIRS   - where to find zlib.h, etc.
#   CGREEN_LIBRARIES      - List of libraries when using zlib.
#   CGREEN_FOUND          - True if zlib found.

find_path(CGREEN_INCLUDE_DIR 
  NAMES cgreen/cgreen.h
)

if(CGREEN_LIBRARY_DIR)
  find_library(CGREEN_LIBRARY
    NAMES cgreen libcgreen
    PATHS ${CGREEN_LIBRARY_DIR})
else()
  find_library(CGREEN_LIBRARY
    NAMES libcgreen)
endif()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CGREEN 
  REQUIRED_VARS CGREEN_LIBRARY CGREEN_INCLUDE_DIR
)

if(CGREEN_FOUND)
  set(CGREEN_LIBRARIES ${CGREEN_LIBRARY})
  set(CGREEN_INCLUDE_DIRS ${CGREEN_INCLUDE_DIR})
endif()

mark_as_advanced(CGREEN_INCLUDE_DIR CGREEN_LIBRARY)

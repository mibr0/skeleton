#include <cgreen/cgreen.h>

// add all tests here
TestSuite *base();
TestSuite *mock_base();
TestSuite *skeleton_version();

int main(int argc, char **argv) {
  TestSuite *suite = create_test_suite();

  add_suite(suite, base());
  add_suite(suite, mock_base());
  add_suite(suite, skeleton_version());

  if (argc > 1)
  {
    return run_single_test(suite, argv[1], create_text_reporter());
  }
  return run_test_suite(suite, create_text_reporter());
}

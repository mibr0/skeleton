#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

void function_with_callback(int value, void (*callback)(int, void*), void* data)
{
  if (value < 1) {
    return;
  }
  for (int i = 0; i < value; ++i) {
    callback(i + 1, 0);
  }
}

Describe(mock_base);
BeforeEach(mock_base) {}
AfterEach(mock_base) {}

void mocked_callback(int size, void* data) 
{
  mock(size, data);
}

Ensure(mock_base, invoke_callback) {
  expect(mocked_callback, when(size, is_equal_to(1)), when(data, is_null));

  function_with_callback(1, &mocked_callback, NULL);
}

Ensure(mock_base, invoke_callback_twice) {
  expect(mocked_callback, when(size, is_equal_to(1)));
  expect(mocked_callback, when(size, is_equal_to(2)));

  function_with_callback(2, &mocked_callback, NULL);
}

TestSuite *mock_base()
{
  TestSuite *suite = create_test_suite();
  add_test_with_context(suite, mock_base, invoke_callback);
  add_test_with_context(suite, mock_base, invoke_callback_twice);
  return suite;
}

#include <cgreen/cgreen.h>
#include <skeleton.h>

Describe(skeleton_version);
BeforeEach(skeleton_version) {}
AfterEach(skeleton_version) {}

Ensure(skeleton_version, major_version) {
  assert_that(skeleton_get_version_major(), is_equal_to(1));
}

TestSuite *skeleton_version()
{
  TestSuite *suite = create_test_suite();
  add_test_with_context(suite, skeleton_version, major_version);
  return suite;
}

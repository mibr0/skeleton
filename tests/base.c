#include <cgreen/cgreen.h>

Describe(base);
BeforeEach(base) {}
AfterEach(base) {}

Ensure(base, passes_this_test) {
  assert_that(1, is_equal_to(1));
}

TestSuite *base()
{
  TestSuite *suite = create_test_suite();
  add_test_with_context(suite, base, passes_this_test);
  return suite;
}

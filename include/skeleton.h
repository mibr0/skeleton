#ifndef SKELETON_H
#define SKELETON_H

#include <stdbool.h>
#include <stdint.h>

///////////////////////////////////////////////////////////////////////////////
// std typedefs
typedef int8_t  int8;
typedef int16_t int16;
typedef int32_t int32;

typedef uint8_t  uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;

///////////////////////////////////////////////////////////////////////////////
// static defines to make it more clear
#define internal static
#define global static
#define local static

///////////////////////////////////////////////////////////////////////////////

int32 skeleton_get_version_major();

#endif // SKELETON_H

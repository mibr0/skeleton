CGLIBFL=CGREEN_LIBRARY_DIR:PATH
CGINCFL=CGREEN_INCLUDE_DIR:PATH

CGLIB=../libs/cgreen/build/build-c/src/
CGINC=../libs/cgreen/include/

all: build
	cd build; make

clean: 
	cd build; make clean

test: init_test
	cd build; make

build:
	mkdir build -p
	cd build; cmake ../

init_test: init_cgreen
	mkdir build -p
	cd build; cmake ../ -D$(CGLIBFL)=$(CGLIB) -D$(CGINCFL)=$(CGINC) -Dskeleton_enable_tests=ON

init_cgreen:
	git submodule init
	git submodule update
	cd libs/cgreen; make

.SILENT:

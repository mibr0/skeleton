#include <skeleton.h>
#include <config.h>

int32 
skeleton_get_version_major()
{
  return SKELETON_VERSION_MAJOR;
}

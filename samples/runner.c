#include <stdio.h>
#include <skeleton.h>

int main(int argc, char** argv)
{
  printf("major version: %d\n", skeleton_get_version_major());
  return 0;
}
